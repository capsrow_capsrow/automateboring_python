#!/usr/bin/env python
# coding: utf-8

name = "Alice"
age = 30

print('My name is %s and I\'m %s years.old' % (name, age))

# f-string, since Python 3.6
print(f'My name {name} %s and I\'m {age} years.old')