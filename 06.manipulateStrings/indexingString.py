#!/usr/bin/env python
# coding: utf-8

spam = "Hello big world !"

for i in range(len(spam)):
    print(spam[i])

print(spam[-1])
print(spam[:5])
print(spam[7:])

print(spam[-2:])
print(spam[:-2])
