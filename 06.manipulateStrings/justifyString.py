#!/usr/bin/env python
# coding: utf-8

spam = "Hello"

print(spam.rjust(20))
print(spam.ljust(20))

print(spam.rjust(20, '*'))
print(spam.ljust(20, '-'))

print(spam.center(20))
print(spam.center(20, '='))