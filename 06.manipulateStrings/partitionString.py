#!/usr/bin/env python
# coding: utf-8

before, sep, after = "Hello, world!".partition(' ')

print("before = " + before)
if sep.isspace():
    print("sep = " + "SPACE sign")
print("after = " + after)