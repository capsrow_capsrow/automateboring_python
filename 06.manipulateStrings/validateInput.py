#!/usr/bin/env python
# coding: utf-8

while True:
    print("Enter your age:")
    age = input()
    if age.isdecimal():
        break
    print("Please enter a number for your age.")

while True:
    print("Select a new passwort (letters and numbers only):")
    password = input()
    if password.isalpha():
        break
    print("Password can only have letters and numbers.")