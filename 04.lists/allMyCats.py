#!/usr/bin/env python
# coding: utf-8

catNames = []

while True:
    print("Enter a name of cat " + str(len(catNames) + 1) + " (or enter nothing to stop):")
    name = input()
    if name == "":
        break
    catNames = catNames + [name]
    print("The cat name is:")
    for name in catNames:
        print(" " + name)