#!/usr/bin/env python
# coding: utf-8

import pyinputplus as pyip

response = pyip.inputNum(allowRegexes=[r'caterpillar', 'category'], blockRegexes=[r'cat'])