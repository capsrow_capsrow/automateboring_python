#!/usr/bin/env python
# coding: utf-8

import pyinputplus as pyip

#accept Roman numerals in addition to the usual numbers
response = pyip.inputNum(allowRegexes=[r'(I|V|X|L|C|D|M)+', r'zero'])