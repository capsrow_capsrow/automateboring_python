#!/usr/bin/env python
# coding: utf-8

import re

beginsWithHello = re.compile(r'^Hello')
print(beginsWithHello.search('Hello, world!'))

if beginsWithHello.search('He said hello.') == None:
    print(beginsWithHello.search('He said hello.'))

endsWithNumber = re.compile(r'\d$')
print(endsWithNumber.search('Your number is 42'))
if endsWithNumber.search('Your number is forty two.') == None:
    print(endsWithNumber.search('Your number is forty two.'))

# The r'^\d+$' regular expression string matches strings that both begin and end with one or more numeric characters
wholeStringIsNum = re.compile(r'^\d+$')
print(wholeStringIsNum.search('1234567890'))
if wholeStringIsNum.search('12345xyz67890') == None:
    print(wholeStringIsNum.search('12345xyz67890'))

if wholeStringIsNum.search('12  34567890') == None:
    print(wholeStringIsNum.search('12  34567890'))